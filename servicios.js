var modulo_configuracion = require('./configuracion.js');  // este modulo contiene toda la informacion de config
const PRIVATE_KEY = require('./emanuel@sentinelprocess-222018.iam.gserviceaccount.com.json');
var ee = require('@google/earthengine');

var obtener_tiles = (mensaje) => {
    //configuramos respuesta...
    let respuesta_obj = new modulo_configuracion.Respuesta(); //Obtenemos un objeto de respuesta
    //configuramos la respuesta
    respuesta_obj.long_SvcProcessId = mensaje["long_SvcProcessId"];
    respuesta_obj.purpose = mensaje["str_purpose"];
    respuesta_obj.str_userId=mensaje["str_userId"];
    respuesta_obj.str_function = mensaje["str_function"];
    ee.data.authenticateViaPrivateKey(PRIVATE_KEY, () => {
        ee.initialize(null, null, () => {
            var geometry = ee.Geometry.Polygon(mensaje["str_geojson_region"]["coordinates"]);
            var imageCollection = ee.ImageCollection("COPERNICUS/S2");
            var image = imageCollection.filterBounds(geometry).sort('system:time_start',false).limit(100);
            var array = image.aggregate_histogram('MGRS_TILE')
            console.log(array);
            //parte particular de la respuesta           
            respuesta_obj.str_output_uri = ee.Dictionary(array).keys().getInfo();
            let respuesta = JSON.stringify(respuesta_obj);
            
            //enviamos la respuesta creada
            queueService.createMessage(modulo_configuracion.configuracion.buzon_escritura, respuesta, function(error) {

             console.log("Enviando mensaje al buzon");
             if (!error) {
                 // Message escrito
                 console.log("mensaje " + respuesta+ " ha sido enviado");
             } else {
                 console.log("Se encontro un error al enviar el mensaje");
             }
                 });


        });
    });
}    

var tasa_cambio =  (msj, sqs) => {

    var mensaje = msj.input;

    // configuramos respuesta...
    let respuesta_obj = new modulo_configuracion.RespuestaTasaCambio(); //Obtenemos un objeto de respuesta

    respuesta_obj.long_SvcProcessId = mensaje["long_SvcProcessId"];
    respuesta_obj.purpose = mensaje["str_purpose"];
    respuesta_obj.str_userId=mensaje["str_userId"];

    respuesta_obj.str_function = mensaje["str_function"];

    ee.data.authenticateViaPrivateKey(PRIVATE_KEY, () => {
        ee.initialize(null, null, (error) => {
       try
       {

// Define geometría

       var lote = ee.Geometry.Polygon(mensaje["str_geojson_region"]["coordinates"]);

            //Define fechas

        var fecha_1 = ee.Date(mensaje["str_start_date"]),
            fecha_2 = ee.Date(mensaje["str_end_date"]);


        
        /////////////////
        
        var fecha1 = ee.Date(fecha_1);
        var fecha2 = ee.Date(fecha_2)
        
        //Define coleccion
        
        var imageCollection = ee.ImageCollection("COPERNICUS/S2")
            .filterBounds(lote);
            
        // obtiene image
        
        var img_fecha1 = ee.Image(imageCollection.filterDate(fecha1,fecha1.advance(1,'day')).mosaic().copyProperties(imageCollection.filterDate(fecha1,fecha1.advance(1,'day')).first()));
        var img_fecha2 = ee.Image(imageCollection.filterDate(fecha2,fecha2.advance(1,'day')).mosaic().copyProperties(imageCollection.filterDate(fecha2,fecha2.advance(1,'day')).first()));
        
        
        // Mascara de nubes 
        
        
        var cloud = img_fecha1.select('QA60').gt(0);
        var fecha1_cloudless = img_fecha1.updateMask(cloud.eq(0))
        
        var cloud2 = img_fecha2.select('QA60').gt(0);
        var fecha2_cloudless = img_fecha2.updateMask(cloud.eq(0))
        
        
        
        // NDVI
        var ndvi1 = fecha1_cloudless.normalizedDifference(['B8','B4']).rename('ndvi1').clip(lote);
        var ndvi2 = fecha2_cloudless.normalizedDifference(['B8','B4']).rename('ndvi2').clip(lote);
        
        // Obtiene la tasa
        
        var dif_days = fecha2.difference(fecha1,'day')
        var tasa = (ndvi2.subtract(ndvi1)).divide(dif_days).rename('tasa').clip(lote)
        
        // resclass para la visualización
        
        var tasa_res = tasa
                            .where(tasa.lt(-0.03),  -0.03 )
                            .where(tasa.gte(-0.03).and(tasa.lt(-0.025)), -0.025)
                            .where(tasa.gte(-0.025).and(tasa.lt(-0.02)), -0.02)
                            .where(tasa.gte(-0.02).and(tasa.lt(-0.015)), -0.015)
                            .where(tasa.gte(-0.015).and(tasa.lt(-0.01)), -0.01)
                            .where(tasa.gte(-0.01).and(tasa.lt(-0.005)), -0.005)
                            .where(tasa.gte(-0.005).and(tasa.lt(0.005)), 0)
                            .where(tasa.gte(0.005).and(tasa.lt(0.01)), 0.005)
                            .where(tasa.gte(0.01).and(tasa.lt(0.015)), 0.01)
                            .where(tasa.gte(0.015).and(tasa.lt(0.02)), 0.015)
                            .where(tasa.gte(0.02).and(tasa.lte(0.025)), 0.02)
                            .where(tasa.gte(0.025).and(tasa.lte(0.03)), 0.025)
                            .where(tasa.gt(0.03), 0.03);
        
        function rescale (ndvi){ var ndvi_rescale = ndvi
                            .where(ndvi.gt(-1).and(ndvi.lte(0)),0)
                            .where(ndvi.gt(0).and(ndvi.lte(0.03)), 0.0454)
                            .where(ndvi.gte(0.03).and(ndvi.lt(0.07)), 0.091)
                            .where(ndvi.gte(0.07).and(ndvi.lt(0.1)), 0.136)
                            .where(ndvi.gte(0.1).and(ndvi.lt(0.13)), 0.182)
                            .where(ndvi.gte(0.13).and(ndvi.lt(0.17)), 0.227)
                            .where(ndvi.gte(0.17).and(ndvi.lt(0.2)), 0.273)
                            .where(ndvi.gte(0.2).and(ndvi.lt(0.25)), 0.318)
                            .where(ndvi.gte(0.25).and(ndvi.lt(0.3)), 0.363)
                            .where(ndvi.gte(0.3).and(ndvi.lt(0.35)), 0.409)
                            .where(ndvi.gte(0.35).and(ndvi.lt(0.4)), 0.454)
                            .where(ndvi.gte(0.4).and(ndvi.lt(0.45)), 0.5)
                            .where(ndvi.gte(0.45).and(ndvi.lt(0.5)), 0.545)
                            .where(ndvi.gte(0.5).and(ndvi.lt(0.55)), 0.591)
                            .where(ndvi.gte(0.55).and(ndvi.lt(0.6)), 0.636)
                            .where(ndvi.gte(0.6).and(ndvi.lt(0.65)), 0.682)
                            .where(ndvi.gte(0.65).and(ndvi.lt(0.7)), 0.727)
                            .where(ndvi.gte(0.7).and(ndvi.lt(0.75)), 0.772)
                            .where(ndvi.gte(0.75).and(ndvi.lt(0.8)), 0.818)
                            .where(ndvi.gte(0.8).and(ndvi.lt(0.85)), 0.864)
                            .where(ndvi.gte(0.85).and(ndvi.lt(0.9)), 0.909)
                            .where(ndvi.gte(0.90).and(ndvi.lt(0.99)), 0.954)
                            .where(ndvi.gte(0.99).and(ndvi.lte(1)), 1)
          return ndvi_rescale
        }
                            
        // parámetros de visualización
        var vis_ndvi = {min: 0, max: 1, palette: ['000000','A0522D','94723C','a4824c','b4966c','c4baa4','94b614','80aa11','6c9f0e',
                    '58930c','448809','307d06','1c7204','467b2d','388457','2a8e81','1c97ab','0ea0d5','00aaff','157fdf','3343b2',
                    '3f2a9f','ffffff']
        };
                        
        var vis_tasa = {min: -0.03, max: 0.03, palette: ['b2182b','d05146','ef8a62','F6B294','fddbc7',
                          'FAE9DF','f7f7f7','E4EEF3','d1e5f0','9CC7DF','67a9cf','4487BD','2166ac']};
        // genera thumbnail
        var thumb_path_tasa = tasa_res.visualize(vis_tasa).reproject({crs:'EPSG:3857',scale:10}).getThumbURL({dimensions:500,region:lote.toGeoJSON()})
        var thumb_path_fecha1 = rescale(ndvi1).visualize(vis_ndvi).reproject({crs:'EPSG:3857',scale:10}).getThumbURL({dimensions:500,region:lote.toGeoJSON()})
        var thumb_path_fecha2 = rescale(ndvi2).visualize(vis_ndvi).reproject({crs:'EPSG:3857',scale:10}).getThumbURL({dimensions:500,region:lote.toGeoJSON()})
        // Area calculation
        
        var areaDict = ee.Image.pixelArea().divide(10000).addBands(tasa_res) // has !
          .reduceRegion({
            reducer: ee.Reducer.sum().group(1),
            geometry: lote, 
            scale: 10,
            maxPixels: 1e10
          });
        
        var feat = ee.List(areaDict.get('groups')).map(function (obj){
                    var area = ee.Number(ee.Dictionary(obj).get('sum')).format('%.2f')
                    var clase = ee.String(ee.Dictionary(obj).get('group'))
                  return ee.Feature(lote,{'clase':clase,'area':area})
        });
        
        var path = ee.FeatureCollection(feat).getDownloadURL('csv',['clase','area'])
        respuesta_obj.tasa_cambio = thumb_path_tasa;
        respuesta_obj.fecha1 = thumb_path_fecha1;
        respuesta_obj.fecha2 = thumb_path_fecha2;
        respuesta_obj.CSV_tasa_cambio = path;


        msj.output= {
            tasa_cambio :respuesta_obj.tasa_cambio,
            thumbnail_fecha1 :respuesta_obj.fecha1,
            thumbnail_fecha2 :respuesta_obj.fecha2,
            str_linkCSV : respuesta_obj.CSV_tasa_cambio
        };

        console.log("Salida definitiva: " + JSON.stringify(msj));

        // Enviar respuesta
        modulo_configuracion.send_queue(sqs,mensaje["str_output_queue"], JSON.stringify(msj))

       } catch(err)
        {

            respuesta_obj.str_output_uri = "Error, no se pudo ejecutar la funcion gee: "+err;
            respuesta_obj.str_gee_email = ee.data.getAuthClientId();

            // Se envía mensaje con error
            msj.output = {};
            msj.output = {
                str_output_err : respuesta_obj.str_output_uri
            };

            modulo_configuracion.send_queue(sqs,mensaje["str_output_queue"], JSON.stringify(msj))
        }

    });
});
}


var dato_serie_ndvi_S2  =  (msj, sqs) => {
    ee.data.authenticateViaPrivateKey(PRIVATE_KEY, () => {
        ee.initialize(null, null, (error) => {

            var mensaje = msj.input;

            //Obtenemos un objeto de respuesta
            let respuesta_obj = new modulo_configuracion.RespuestaSerieNdviS2();

            try {
                ///////////////  VARIABLES

///////// GEOMETRIA
                // Define geometría

                var table = ee.Geometry.Polygon(mensaje["str_geojson_region"]["coordinates"]);

//Define fechas

                var fechain = ee.Date(mensaje["str_start_date"]),
                    fechafin = ee.Date(mensaje["str_end_date"]);

///////////////



                var S2 = ee.ImageCollection('COPERNICUS/S2')
                    .filterDate(fechain, fechafin)
                    .filterBounds(table)

// MOSAIC

                var list_dates = ee.Dictionary(S2.aggregate_histogram('system:time_start')).keys()

                var S2_mosaic = list_dates.map (function (date){
                    var dates = ee.Number.parse(date)
                    var image = S2.filterDate(ee.Date(dates),ee.Date(dates).advance(1,'day'))
                    return ee.ImageCollection(image).mosaic().set({'system:time_start':dates})
                });

// NDVI

                var NDVI = ee.ImageCollection(S2_mosaic).map (function (image){
                    var ndvi = image.normalizedDifference(['B8','B4']).copyProperties(image,['system:time_start'])
                    var score = image.select('B2').multiply(image.select('B9'))//.multiply(1e6);
                    var cloudScoreThreshold = 850000;
                    var cloud = score.lt(cloudScoreThreshold);
                    return ee.Image(ndvi).updateMask(cloud).rename(['ndvi'])
                });




                var vis = {min: 0, max: 1, palette: [
                        'FFFFFF', 'CE7E45', 'FCD163', '66A000', '207401',
                        '056201', '004C00', '023B01', '012E01', '011301'
                    ]};


////////////////////////////////   INTERPOLATE    ///////////////////////////////////////

                var addTimeBand = function(image) {
                    return image.addBands(image.metadata('system:time_start').rename("time"))
                };
                var annualSR = NDVI.map(addTimeBand)
                var time = 'system:time_start'
                var maxDiff = ee.Filter.maxDifference(15 * (1000*60*60*24), time, null, time)
                var f1 = ee.Filter.and(ee.Filter.lessThanOrEquals(time, null, time))
                var c1 = ee.Join.saveAll('after', time, false).apply(annualSR, annualSR, f1)
                var f2 = ee.Filter.and(maxDiff,ee.Filter.greaterThanOrEquals(time, null, time))
                var c2 = ee.Join.saveAll('before', time, true).apply(c1, c1, f2)
                var interpolated = ee.ImageCollection(c2.map(function(img) {
                    img = ee.Image(img);
                    var before = ee.ImageCollection.fromImages(ee.List(img.get('before'))).mosaic()
                    var after = ee.ImageCollection.fromImages(ee.List(img.get('after'))).mosaic()
                    var x1 = before.select('time').double()
                    var x2 = after.select('time').double()
                    var now = ee.Image.constant(img.date().millis()).double();
                    var ratio = now.subtract(x1).divide(x2.subtract(x1))

                    var interp = after.subtract(before).multiply(ratio).add(before)
                    return interp.set('system:time_start', img.get('system:time_start'));
                }))


/////////////////////////////// Smoothing ---------------------------------------------------------------

                var modeled = interpolated.select('ndvi')
                var timeField = 'system:time_start'

                var join = ee.Join.saveAll({
                    matchesKey: 'images'
                });

                var diffFilter = ee.Filter.maxDifference({
                    difference: 1000 * 60 * 60 * 24 * 17,
                    leftField: timeField,
                    rightField: timeField
                });

                var threeNeighborJoin = join.apply({
                    primary: modeled,
                    secondary: modeled,
                    condition: diffFilter
                });

                var smoothed = ee.ImageCollection(threeNeighborJoin.map(function(image) {
                    var collection = ee.ImageCollection.fromImages(image.get('images'));
                    return ee.Image(image).addBands(collection.mean().rename('mean'));
                }));


                var chart_data = smoothed.select('mean').map (function (image) {
                    var mean = image.reduceRegion(ee.Reducer.mean(),table,500)
                    var time = ee.String('2018-').cat(ee.String(ee.Date(image.get('system:time_start')).format('MM-dd')))
                    var serie = ee.String(ee.Date(image.get('system:time_start')).format('yyyy'))
                    return ee.Feature(table,mean).set({'ndvi':mean.get('mean'),'time':ee.Date(time), 'serie':serie})
                });


                respuesta_obj.CSV_ndvi = ee.FeatureCollection(chart_data).getDownloadURL('csv',['ndvi','serie','time']);
                respuesta_obj.long_SvcProcessId = mensaje["long_SvcProcessId"];
                respuesta_obj.str_function = mensaje["str_function"];

                msj.output= {
                    str_linkCSV : respuesta_obj.CSV_ndvi
                };
                console.log("Salida definitiva: " + JSON.stringify(msj));

                // Enviar respuesta
                modulo_configuracion.send_queue(sqs,mensaje["str_output_queue"], JSON.stringify(msj))
            }
            catch (e) {

                respuesta_obj.str_output_uri = "Error, no se pudo ejecutar la funcion gee: "+e;
                console.log(JSON.stringify(respuesta_obj));

                // Se envía mensaje con error
                msj.output = {};
                msj.output = {
                    str_output_err : respuesta_obj.str_output_uri
                };

                modulo_configuracion.send_queue(sqs,mensaje["str_output_queue"], JSON.stringify(msj))
            }
        });
    });
}

var dato_serie_temp_NCEP  =  (msj, sqs) => {
    ee.data.authenticateViaPrivateKey(PRIVATE_KEY, () => {
        ee.initialize(null, null, (error) => {

            var mensaje = msj.input;

            //Obtenemos un objeto de respuesta
            let respuesta_obj = new modulo_configuracion.RespuestaSerieTempNCEP();

            try {

                var table = ee.Geometry.Polygon(mensaje["str_geojson_region"]["coordinates"]);

                var TEMP = ee.ImageCollection("NCEP_RE/surface_temp")
                    .filterDate('2015-05-01', Date.now())
                    .filterBounds(table);

                function imgcol_last(imgcol){
                    // ee.Image(imgcol_grace.reduce(ee.Reducer.last())); properties are missing
                    return ee.Image(imgcol.toList(1, imgcol.size().subtract(1)).get(0));
                }

                function aggregate_prop(ImgCol, prop, reducer, delta){
                    if (typeof reducer === 'undefined') {reducer = 'mean'}
                    if (typeof delta   === 'undefined') {delta   = false}

                    var dates = ee.Dictionary(ImgCol.aggregate_histogram(prop)).keys()
                        .map(function(p){
                            return ee.Image(0).set(prop, p).set('system:id', p);
                        });

                    var filterDateEq = ee.Filter.equals({ leftField : prop, rightField: prop});
                    var saveAllJoin = ee.Join.saveAll({
                        matchesKey: 'matches',
                        ordering  : 'system:time_start',
                        ascending : true
                    });
                    var ImgCol_new = saveAllJoin.apply(dates, ImgCol, filterDateEq)

                        .map(function(img){
                            img = ee.Image(img);
                            var imgcol = ee.ImageCollection.fromImages(img.get('matches')).sort('system:time_start'); //.fromImages

                            var first = ee.Image(imgcol.first());
                            var last  = imgcol_last(imgcol);

                            var res = ee.Algorithms.If(delta, last.subtract(first), imgcol.reduce(reducer))
                            return ee.Image(res)
                                .copyProperties(ee.Image(imgcol.first()),
                                    ['Year', 'YearStr', 'YearMonth', 'Month', 'Season', 'dn', 'system:time_start'])
                                .copyProperties(img, ['system:id', prop]);
                        });
                    return ee.ImageCollection(ImgCol_new);
                }


                var temp_air = TEMP.map(function(image) {// menos 273.15 Celsius
                    var temp = image.rename(['Temp_']).subtract(273.15).copyProperties(image,['system:time_start'])
                    var mask = ee.Image(temp).lt(330)
                    var date  = ee.Date(image.get('system:time_start'));
                    var day = date.get('day');
                    var month = date.get('month');
                    var year  = date.get("year").format("%d");
                    var month_flag = month//.subtract(1).divide(2).floor().add(1);
                    var month2_flag = month//.subtract(1).divide(2).floor().add(1);
                    var day_flag = day.format('-%02d')
                    month_flag = month_flag.format('-%02d');
                    month2_flag = month2_flag.format('-%02d');

                    return ee.Image(temp).updateMask(mask)
                        .set('day', day)
                        .set('month', month)
                        .set('month_flag', year.cat(month_flag))
                        .set('day_flag', year.cat(month2_flag).cat(day_flag));
                });

                var prop = 'month_flag'
                var reducer = 'min';

                var temp_month_min = aggregate_prop(temp_air, prop, reducer);
                var temp_month_max = aggregate_prop(temp_air, prop, 'max');
                var temp_month =temp_month_min.combine(temp_month_max);

                var featColl = temp_month.map (function (image) {
                    var temp = image.reduceRegion(ee.Reducer.mean(),table,5000)
                    var time = ee.String(ee.Date(image.get('system:time_start')).format('yyyy-MM-dd'))
                    var serie = ee.String(ee.Date(image.get('system:time_start')).format('yyyy'))
                    return ee.Feature(table).set({'temp_max':temp.get('Temp__max'),'temp_min':temp.get('Temp__min'),'time':ee.Date(time), 'serie':serie})
                });

                respuesta_obj.CSV_temp = ee.FeatureCollection(featColl).getDownloadURL('csv',['temp_max','temp_min','serie','time']);
                respuesta_obj.long_SvcProcessId = mensaje["long_SvcProcessId"];
                respuesta_obj.str_function = mensaje["str_function"];

                msj.output= {};
                msj.output= {
                    str_linkCSV : respuesta_obj.CSV_temp
                };
                console.log("Salida definitiva: " + JSON.stringify(msj));

                // Enviar respuesta
                modulo_configuracion.send_queue(sqs,mensaje["str_output_queue"], JSON.stringify(msj));

            }
            catch (e) {
                respuesta_obj.str_output_uri = "Error, no se pudo ejecutar la funcion gee: "+e;
                console.log(JSON.stringify(respuesta_obj));

                // Se envía mensaje con error
                msj.output = {};
                msj.output = {
                    str_output_err : respuesta_obj.str_output_uri
                };

                modulo_configuracion.send_queue(sqs,mensaje["str_output_queue"], JSON.stringify(msj))
            }
        });
    });
}



var serie_ndvi_thumbnail_s2 =  (msj, sqs) => {

    var mensaje = msj.input;
    // configuramos respuesta...
    let respuesta_obj = new modulo_configuracion.RespuestaTasaCambio(); //Obtenemos un objeto de respuesta
    respuesta_obj.long_SvcProcessId = mensaje["long_SvcProcessId"];
    respuesta_obj.purpose = mensaje["str_purpose"];
    respuesta_obj.str_userId=mensaje["str_userId"];
    respuesta_obj.str_function = mensaje["str_function"];
    ee.data.authenticateViaPrivateKey(PRIVATE_KEY, () => {
        ee.initialize(null, null, (error) => {
       try
       {
            // Define geometría
            var geometry = ee.Geometry.Polygon(mensaje["str_geojson_region"]["coordinates"]);
            //Define fechas
            var fechain = ee.Date(mensaje["str_start_date"]),
            fechafin = ee.Date(mensaje["str_end_date"]);
            /////////// COLLECTION
            var images = ee.ImageCollection("COPERNICUS/S2")
                        .filterDate(fechain,fechafin)
                        .filterBounds(geometry);
                        
            var dates = images.map (function (image){
                        var date = ee.Date(image.get('system:time_start')).format('yyyy-MM-dd');
                        return ee.Feature(null,{'date':date})
            });

            //print (ee.List(ee.FeatureCollection(dates).aggregate_array('date')))
            ////////// THUMBNAIL GENERATOR
            var table = function thumb_gen (date_list){
            var image = date_list.map(function (dates){
                    var image = images.filterDate(ee.Date(dates),ee.Date(dates).advance(1,'day')).select(['QA60','B8','B4','B3','B2']).first()
                    var cloud = image.select('QA60').gt(0);
                    var imagecloudFree = image.updateMask(cloud.eq(0))
                    var ndvi = imagecloudFree.normalizedDifference(['B8','B4']).rename('NDVI')
                    var ndvivalue = ndvi.reduceRegion(ee.Reducer.mean(),geometry,10)
                    var ndvi_rescale = ndvi.unmask(1)
                                .where(ndvi.gt(-1).and(ndvi.lte(0)),0)
                                .where(ndvi.gt(0).and(ndvi.lte(0.03)), 0.0454)
                                .where(ndvi.gte(0.03).and(ndvi.lt(0.07)), 0.091)
                                .where(ndvi.gte(0.07).and(ndvi.lt(0.1)), 0.136)
                                .where(ndvi.gte(0.1).and(ndvi.lt(0.13)), 0.182)
                                .where(ndvi.gte(0.13).and(ndvi.lt(0.17)), 0.227)
                                .where(ndvi.gte(0.17).and(ndvi.lt(0.2)), 0.273)
                                .where(ndvi.gte(0.2).and(ndvi.lt(0.25)), 0.318)
                                .where(ndvi.gte(0.25).and(ndvi.lt(0.3)), 0.363)
                                .where(ndvi.gte(0.3).and(ndvi.lt(0.35)), 0.409)
                                .where(ndvi.gte(0.35).and(ndvi.lt(0.4)), 0.454)
                                .where(ndvi.gte(0.4).and(ndvi.lt(0.45)), 0.5)
                                .where(ndvi.gte(0.45).and(ndvi.lt(0.5)), 0.545)
                                .where(ndvi.gte(0.5).and(ndvi.lt(0.55)), 0.591)
                                .where(ndvi.gte(0.55).and(ndvi.lt(0.6)), 0.636)
                                .where(ndvi.gte(0.6).and(ndvi.lt(0.65)), 0.682)
                                .where(ndvi.gte(0.65).and(ndvi.lt(0.7)), 0.727)
                                .where(ndvi.gte(0.7).and(ndvi.lt(0.75)), 0.772)
                                .where(ndvi.gte(0.75).and(ndvi.lt(0.8)), 0.818)
                                .where(ndvi.gte(0.8).and(ndvi.lt(0.85)), 0.864)
                                .where(ndvi.gte(0.85).and(ndvi.lt(0.9)), 0.909)
                                .where(ndvi.gte(0.90).and(ndvi.lt(0.99)), 0.954)
                                .where(ndvi.gte(0.99).and(ndvi.lte(1)), 1)
                    var ndvi_reproj = ndvi_rescale.reproject({crs:'EPSG:3857'})
                    var url = ndvi_reproj.clip(geometry).getThumbURL({
                        bands: ["NDVI"]
                        ,min: 0, max: 1, palette: ['000000','A0522D','94723C','a4824c','b4966c','c4baa4','94b614','80aa11','6c9f0e',
                        '58930c','448809','307d06','1c7204','467b2d','388457','2a8e81','1c97ab','0ea0d5','00aaff','157fdf','3343b2',
                        '3f2a9f','ffffff']
                    })
                    //print (dates)
                    //print (url)
                    //print ('NDVI:',ndvivalue.get('NDVI'))
                    return ee.Feature(null).set({'date':dates,'NDVI':ndvivalue.get('NDVI'),'url':url})
            });
            return image
            }

           //////////   EVALUATE CALLBACK
           var date_list = ee.List(ee.FeatureCollection(dates).aggregate_array('date')).getInfo();
           var resultado = table(date_list)
           var csv_link = ee.FeatureCollection(resultado).getDownloadURL('csv',['date','NDVI','url'])
           respuesta_obj.serie_ndvi_csv = csv_link;
           msj.output= {
               serie_ndvi_csv :respuesta_obj.serie_ndvi_csv
           };

           console.log("Salida definitiva: " + JSON.stringify(msj));

           // Enviar respuesta
           modulo_configuracion.send_queue(sqs,mensaje["str_output_queue"], JSON.stringify(msj))

       } catch(err)
        {

        respuesta_obj.str_output_uri = "Error, no se pudo ejecutar la funcion gee: "+err;
        respuesta_obj.str_gee_email = ee.data.getAuthClientId();

        // Se envía mensaje con error
        msj.output = {};
        msj.output = {
            str_output_err : respuesta_obj.str_output_uri
        };

        modulo_configuracion.send_queue(sqs,mensaje["str_output_queue"], JSON.stringify(msj))
    }

    });
});
}



exports.obtener_tiles = obtener_tiles
exports.tasa_cambio = tasa_cambio
exports.dato_serie_ndvi_S2 = dato_serie_ndvi_S2
exports.dato_serie_temp_NCEP = dato_serie_temp_NCEP
exports.serie_ndvi_thumbnail_s2 = serie_ndvi_thumbnail_s2


