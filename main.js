var modulo_configuracion = require('./configuracion.js');  // este modulo contiene toda la informacion de config
var modulo_servicios = require('./servicios.js');  // este modulo contiene todos los servicios de GEE disponibles para ejecucion.
var ee = require('@google/earthengine');
var fs = require('fs');
var modulo_amazon = require ('./buzones/modulo_amazon/buzones.js');
var aws      = require('aws-sdk');
aws.config.loadFromPath('./buzones/config.json');

// Instantiate SQS.
var sqs = new aws.SQS();

  // Cada mensaje del buzon llama a una, y solo una funcion.

// Función que lee del buzón y ejecuta scripts GEE según corresponda
async function bucleAnalysis() {
    console.log("Ejecutando...");
    let message = await
        modulo_configuracion.read_queue(sqs, "https://sqs.us-west-2.amazonaws.com/942573785109/gee_functions")
    var obj_message = JSON.parse(message.Body);

    // switch que ejecuta el servicio solicitado en el mensaje
    switch (obj_message.input.str_function) {
        case "obtener_tiles": {
            modulo_servicios.obtener_tiles(obj_message);
            break;
        }
        case "tasa_cambio": {
            modulo_servicios.tasa_cambio(obj_message, sqs);
            break;
        }
        case "dato_serie_ndvi_S2": {
            modulo_servicios.dato_serie_ndvi_S2(obj_message, sqs);
            break;
        }
        case "dato_serie_temp_NCEP": {
            modulo_servicios.dato_serie_temp_NCEP(obj_message, sqs);
            break;
        }
        case "serie_ndvi_thumbnail_s2": {
            modulo_servicios.serie_ndvi_thumbnail_s2(obj_message, sqs);
            break;
        }

    }
}

// Se ejecuta bucleAnalysis cada 5 segundos.
setInterval(bucleAnalysis, 5000);


