var configuracion = {	
	//configura credenciales de azure storage
    buzon_lectura : "geo-py-dev-sd",
    buzon_escritura : "geo-webjob-dev-sd",
    STORAGE_ACCOUNT_NAME:"geoagrotest1",	
    STORAGE_ACCOUNT_KEY: "ykG8ENBAmxOqzljqJ/FGPBhzKrYJTcIFIHXxCA60Lyh9Y30NnKDMN92F7tWVBzOBjCV7jFKj4arAUk4G06AEMQ=="
}				
//credenciales DBMS 
const connectionData = {
    user: 'isachiara',
    host: 'geemonitor.cnctx1hsf5fm.us-east-1.rds.amazonaws.com',
    database: 'geoagro1',
    password: 'Rc4nob0Rc4nob0',
    port: 5432,
 }

  //credenciales DBMS
const connectionData_geotiler = {
    user: 'isachiara',
    host: 'geemonitor.cnctx1hsf5fm.us-east-1.rds.amazonaws.com',
    database: 'geotiler',
    password: 'Rc4nob0Rc4nob0',
    port: 5432,
  }


//dado un mensaje queueUrl y un elemento de conexion sqs  envia un mensaje a la cola.

let send_queue = function send_queue_data(sqs,queueUrl,message)
{
  return new Promise( async function(resolve, reject) { 

      var params = {
        MessageBody: message,
        QueueUrl: queueUrl,
        DelaySeconds: 0
    };
    sqs.sendMessage(params, function(err, data) {
        if(err) {
            reject(err)
        } 
        else {
            resolve(data);
        } 
    });
  });
}
//dado un queueUrl y un elemento de conexion sqs, devuelve un mensaje y lo elimina de la cola.
var read_queue = function (sqs,queueUrl) {

    return new Promise( async function(resolve, reject) { 


    var params = {
        QueueUrl: queueUrl,
        VisibilityTimeout: 600 // 10 min wait time for anyone else to process.
    };
    sqs.receiveMessage(params, function(err, data) {
        if(err) {
           reject(err); // error al leer
        } 
        else if (data.Messages) {   // si existen los mensajes
            // creo los parametros para eliminar
            let message = data.Messages[0]
            var params ={
                QueueUrl: queueUrl,
                ReceiptHandle : data.Messages[0].ReceiptHandle               
            }
            sqs.deleteMessage(params, function(err, data){

                if(err){
                    reject(err);   // error de eliminacions
                }else{
                    console.log(data);
                    console.log("Leyendo mensaje: " +message.Body);
                }
            });
            resolve(message);
        }
    });
});
}
  
//configuracion de respuesta
class Respuesta
{

    constructor(str_direction='output',long_SvcProcessId=null,purpose=null,long_ReportId=null,
                long_frmVersionId=null,str_userId=null,str_function=null,str_farm_name=null,str_gee_email=null,
                str_output_uri=nul) {
        this.str_direction=str_direction;
        this.long_SvcProcessId=long_SvcProcessId; // esto es para ver dada una respuesta a que pedido pertenece
        this.purpose=purpose;
        this.long_ReportId=long_ReportId;
        this.long_frmVersionId=long_frmVersionId; //preguntar a la T
        this.str_userId=str_userId;
        this.str_function=str_function;
        this.str_farm_name=str_farm_name; //preguntar a la T
        this.str_gee_email=str_gee_email; 
        this.str_output_uri=str_output_uri;

      }


}

// Respuesta tasa_cambio
class RespuestaTasaCambio
{

    constructor(str_direction='output',long_SvcProcessId=null,purpose=null,long_ReportId=null,
                long_frmVersionId=null,str_userId=null,str_function=null,str_farm_name=null,str_gee_email=null,
                str_output_uri=null,tasa_cambio=null,fecha1=null,fecha2=null,CSV_tasa_cambio=null) {
        this.str_direction=str_direction;
        this.long_SvcProcessId=long_SvcProcessId; // esto es para ver dada una respuesta a que pedido pertenece
        this.purpose=purpose;
        this.long_ReportId=long_ReportId;
        this.long_frmVersionId=long_frmVersionId; //preguntar a la T
        this.str_userId=str_userId;
        this.str_function=str_function;
        this.str_farm_name=str_farm_name; //preguntar a la T
        this.str_gee_email=str_gee_email;
        this.str_output_uri=str_output_uri;

        // tasa_cambio
        this.tasa_cambio=tasa_cambio;
        this.fecha1=fecha1;
        this.fecha2=fecha2;
        this.CSV_tasa_cambio=CSV_tasa_cambio;

    }
}

// Respuesta dato_serie_ndvi_S2
class RespuestaSerieNdviS2
{

    constructor(str_direction='output',long_SvcProcessId=null,purpose=null,long_ReportId=null,
                long_frmVersionId=null,str_userId=null,str_function=null,str_farm_name=null,str_gee_email=null,
                str_output_uri=null,CSV_ndvi=null) {
        this.str_direction=str_direction;
        this.long_SvcProcessId=long_SvcProcessId; // esto es para ver dada una respuesta a que pedido pertenece
        this.purpose=purpose;
        this.long_ReportId=long_ReportId;
        this.long_frmVersionId=long_frmVersionId; //preguntar a la T
        this.str_userId=str_userId;
        this.str_function=str_function;
        this.str_farm_name=str_farm_name; //preguntar a la T
        this.str_gee_email=str_gee_email;
        this.str_output_uri=str_output_uri;

        // dato_serie_ndvi_S2
        this.CSV_ndvi = CSV_ndvi;
    }
}

class RespuestaSerieTempNCEP
{

    constructor(str_direction='output',long_SvcProcessId=null,purpose=null,long_ReportId=null,
                long_frmVersionId=null,str_userId=null,str_function=null,str_farm_name=null,str_gee_email=null,
                str_output_uri=null,CSV_temp=null) {
        this.str_direction=str_direction;
        this.long_SvcProcessId=long_SvcProcessId; // esto es para ver dada una respuesta a que pedido pertenece
        this.purpose=purpose;
        this.long_ReportId=long_ReportId;
        this.long_frmVersionId=long_frmVersionId; //preguntar a la T
        this.str_userId=str_userId;
        this.str_function=str_function;
        this.str_farm_name=str_farm_name; //preguntar a la T
        this.str_gee_email=str_gee_email;
        this.str_output_uri=str_output_uri;

        // dato_serie_temp_NCEP
        this.CSV_temp = CSV_temp;
    }
}


module.exports = {
    configuracion: configuracion,
    Respuesta: Respuesta,
    RespuestaTasaCambio: RespuestaSerieNdviS2,
    RespuestaSerieNdviS2: RespuestaSerieNdviS2,
    RespuestaSerieTempNCEP: RespuestaSerieTempNCEP,
    connectionData: connectionData,
    connectionData_geotiler: connectionData_geotiler,
    send_queue: send_queue,
    read_queue: read_queue
}

/*exports.configuracion = configuracion
exports.Respuesta = Respuesta
exports.RespuestaTasaCambio = RespuestaTasaCambio
exports.RespuestaSerieNdviS2 = RespuestaSerieNdviS2
exports.connectionData = connectionData
exports.connectionData_geotiler = connectionData_geotiler
exports.send_queue = send_queue
exports.read_queue = read_queue*/

