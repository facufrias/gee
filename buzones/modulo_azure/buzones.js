//const EventEmitter = require('events'); // Necesario para trabajar con eventos y funciones sincronas.
//var azure = require('azure-storage'); // necesario para conectar a azure
var modulo_configuracion = require('../configuracion.js');  // este modulo contiene toda la informacion de config
var modulo_servicios = require('./servicios.js');  // este modulo contiene todos los servicios de GEE disponibles para ejecucion.
//var ee = require('@google/earthengine');



//class azureInterface_lectura extends EventEmitter {}

//const azureInterface_obj = new azureInterface_lectura()

//modulo_configuracion.configuracion.buzon_lectura = "geo-py-dev-sd";

//modulo_configuracion.configuracion.buzon_escritura = "geo-webjob-dev-sd";


let send_queue = function send_queue_data(message, queueName)
{
  return new Promise( async function(resolve, reject) { 

    modulo_configuracion.configuracion.buzon_escritura = queueName;

    queueUrl = modulo_configuracion.configuracion.buzon_escritura;


    modulo_servicios.azureInterface_obj.emit("escribir_buzon",queueUrl,message, function(err, data) 
    {
       
      if(err) {
          console.log(err)
          reject(err)
      } 
      else {
          resolve(data);
        } 
      });

  });
}



let read_queue = function read_queue_data(queueName)
{
  return new Promise( async function(resolve, reject) { 

    modulo_configuracion.configuracion.buzon_escritura = queueName;

    queueUrl = modulo_configuracion.configuracion.buzon_escritura;


    modulo_servicios.azureInterface_obj.emit("leer_buzon",queueUrl, function(err, data) 
    {
       
      if(err) {
          console.log(err)
          reject(err)
      } 
      else {
          resolve(data);
        } 
      });

  });
}


exports.read_queue = read_queue;

exports.send_queue = send_queue;
