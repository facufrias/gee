const EventEmitter = require('events'); // Necesario para trabajar con eventos y funciones sincronas.

//var modulo_configuracion = require('./configuracion.js');  // este modulo contiene toda la informacion de config
var modulo_configuracion = require('../configuracion.js');  // este modulo contiene toda la informacion de config

var azure = require('azure-storage'); // necesario para conectar a azure


class azureInterface_escritura extends EventEmitter {}
const azureInterface_obj = new azureInterface_escritura()
const PRIVATE_KEY = require('./emanuel@sentinelprocess-222018.iam.gserviceaccount.com.json');
var ee = require('@google/earthengine');
var queueService = azure.createQueueService(modulo_configuracion.configuracion.STORAGE_ACCOUNT_NAME, modulo_configuracion.configuracion.STORAGE_ACCOUNT_KEY);


//este evento escribe un mensaje dado en el buzon queueName
azureInterface_obj.on('escribir_buzon', function(queueName,mensaje) 
{

    var queueService = azure.createQueueService(modulo_configuracion.configuracion.STORAGE_ACCOUNT_NAME, modulo_configuracion.configuracion.STORAGE_ACCOUNT_KEY);

    queueService.createMessage(queueName, mensaje, function(error) {
        console.log("Enviando mensaje al buzon" + queueName);

        if (!error) {
         // Message escrito
         console.log("mensaje " + mensaje+ " ha sido enviado");
        }else{
           console.log("Se encontro un error al enviar el mensaje");
        }
    });
});


azureInterface_obj.on('leer_buzon',function(queueName)
{

    var queueService = azure.createQueueService(modulo_configuracion.configuracion.STORAGE_ACCOUNT_NAME, modulo_configuracion.configuracion.STORAGE_ACCOUNT_KEY);

    queueService.getMessage(queueName, function(error,serverMessage) {

        console.log("Leyendo mensaje del buzon" + queueName);

        if (!error) {
         // Message leido
         console.log("mensaje leido"); 
		 console.log(serverMessage.messageText);

         // elimino el mensaje leido ..

            queueService.deleteMessage(queueName,serverMessage.messageId, serverMessage.popReceipt, function(error){

               if(!error){
                   //mensaje eliminado
                   console.log("mensaje leido y eliminado");
                }

            });


        }else{
           console.log("error al leer buzon .");
        }
    });


})


var obtener_tiles = (mensaje) => {
    //configuramos respuesta...
    let respuesta_obj = new modulo_configuracion.Respuesta(); //Obtenemos un objeto de respuesta
    //configuramos la respuesta
    respuesta_obj.long_SvcProcessId = mensaje["long_SvcProcessId"];
    respuesta_obj.purpose = mensaje["str_purpose"];
    respuesta_obj.str_userId=mensaje["str_userId"];
    respuesta_obj.str_python_function = mensaje["str_python_function"];
    ee.data.authenticateViaPrivateKey(PRIVATE_KEY, () => {
        ee.initialize(null, null, () => {
            var geometry = ee.Geometry.Polygon(mensaje["str_geojson_region"]["coordinates"]);
            var imageCollection = ee.ImageCollection("COPERNICUS/S2");
            var image = imageCollection.filterBounds(geometry).sort('system:time_start',false).limit(100);
            var array = image.aggregate_histogram('MGRS_TILE')
            console.log(array);
            //parte particular de la respuesta           
            respuesta_obj.str_output_uri = ee.Dictionary(array).keys().getInfo();
            let respuesta = JSON.stringify(respuesta_obj);
            
            //enviamos la respuesta creada
            queueService.createMessage(modulo_configuracion.configuracion.buzon_escritura, respuesta, function(error) {

                console.log("Enviando mensaje al buzon");
                if (!error) {   
                    // Message escrito
                    console.log("mensaje " + respuesta+ " ha sido enviado");
                }else{
                    console.log("Se encontro un error al enviar el mensaje");
                }
                    });


        });
    });
}    

var tasa_cambio =  (mensaje) => {	

    
    //configuramos respuesta...
    let respuesta_obj = new modulo_configuracion.Respuesta(); //Obtenemos un objeto de respuesta
    //configuramos la respuesta
    respuesta_obj.long_SvcProcessId = mensaje["long_SvcProcessId"];
    respuesta_obj.purpose = mensaje["str_purpose"];
    respuesta_obj.str_userId=mensaje["str_userId"];

    respuesta_obj.str_python_function = mensaje["str_python_function"];

    ee.data.authenticateViaPrivateKey(PRIVATE_KEY, () => {
        ee.initialize(null, null, (error) => {
        console.log()
       try
       {
        var lote = ee.Geometry.Polygon(mensaje["str_geojson_region"]["coordinates"]);
        //Define fechas
        var fecha_1 = mensaje["str_start_date"];
        var fecha_2 = mensaje["str_end_date"];
      

       
        var fecha1 = ee.Date(fecha_1);
        var fecha2 = ee.Date(fecha_2)

        //Define coleccion
        var imageCollection = ee.ImageCollection("COPERNICUS/S2").filterBounds(lote);

        var img_size = imageCollection.filterDate(fecha1,fecha1.advance(1,'day')).filterBounds(lote).size().getInfo()
        if(img_size != 0)
        {
                // obtiene image
                var img_fecha1 = ee.Image(imageCollection.filterDate(fecha1,fecha1.advance(1,'day')).mosaic().copyProperties(imageCollection.filterDate(fecha1,fecha1.advance(1,'day')).first()));
                var img_fecha2 = ee.Image(imageCollection.filterDate(fecha2,fecha2.advance(1,'day')).mosaic().copyProperties(imageCollection.filterDate(fecha2,fecha2.advance(1,'day')).first()));
                // Mascara de nubes y sombra
                function sentinelCloudScore(toa) {
                var score = toa.select('B2').multiply(toa.select('B9')).multiply(1e4);
                var cloudScoreThreshold = 450;
                var cloud = score.gt(cloudScoreThreshold);
                return cloud;
                }
            
                function shadowMask(toa,cloud){
                var azi = ee.Number(toa.get('solar_azimuth'));
                var zen = ee.Number(toa.get('solar_zenith')).multiply(1.6);
                var azimuth =azi.multiply(Math.PI).divide(180.0).add(ee.Number(0.5).multiply(Math.PI));
                var zenith  =ee.Number(0.5).multiply(Math.PI ).subtract(zen.multiply(Math.PI).divide(180.0));
                var nominalScale = cloud.projection().nominalScale();
                var cloudHeights = ee.List.sequence(200,5000,500);
                function cloudH (cloudHeight){
                cloudHeight = ee.Number(cloudHeight);
                var shadowVector = zenith.tan().multiply(cloudHeight);
                var x = azimuth.cos().multiply(shadowVector).divide(nominalScale).round();
                var y = azimuth.sin().multiply(shadowVector).divide(nominalScale).round();
                return cloud.changeProj(cloud.projection(), cloud.projection().translate(x, y));
                }
                var shadows = cloudHeights.map(cloudH);
                var potentialShadow = ee.ImageCollection.fromImages(shadows).max();
                var potentialShadow1 = potentialShadow.and(cloud.not());
                var darkPixels = toa.select(['B8','B11','B12']).reduce(ee.Reducer.sum()).multiply(1e3).lt(250).rename(['dark_pixels']);
                var shadow = potentialShadow1.and(darkPixels).rename('shadows');
                return shadow;
                }
            
                function sentinel2toa(img) {
                var toa = img//.select(['B4','B8','B2','B9','B11','B12'])  
                .divide(10000)
                .set('solar_azimuth',img.get('MEAN_SOLAR_AZIMUTH_ANGLE'))
                .set('solar_zenith', img.get('MEAN_SOLAR_ZENITH_ANGLE'));
                return toa;
                }
            
                function cloud_and_shadow_mask(img) {
                    var toa = sentinel2toa(img);
                    var cloud = sentinelCloudScore(toa);
                    var shadow = shadowMask(toa,cloud);
                    var mask = cloud.or(shadow).fastDistanceTransform(75, 'pixels').gt(75);
                    return toa.updateMask(mask);
                }
                var fecha1_cloudless = cloud_and_shadow_mask(img_fecha1)
                var fecha2_cloudless = cloud_and_shadow_mask(img_fecha2)
                // NDVI
                var ndvi1 = fecha1_cloudless.normalizedDifference(['B8','B4']).rename('ndvi1');
                var ndvi2 = fecha2_cloudless.normalizedDifference(['B8','B4']).rename('ndvi2');
            
                // Obtiene la tasa
            
                var dif_days = fecha2.difference(fecha1,'day')
                var tasa = (ndvi2.subtract(ndvi1)).divide(dif_days).rename('tasa').clip(lote)   
                // resclass para la visualización
                var tasa_res = tasa
                                .where(tasa.lt(-0.03),  -0.03 )
                                .where(tasa.gte(-0.03).and(tasa.lt(-0.025)), -0.025)
                                .where(tasa.gte(-0.025).and(tasa.lt(-0.02)), -0.02)
                                .where(tasa.gte(-0.02).and(tasa.lt(-0.015)), -0.015)
                                .where(tasa.gte(-0.015).and(tasa.lt(-0.01)), -0.01)
                                .where(tasa.gte(-0.01).and(tasa.lt(-0.005)), -0.005)
                                .where(tasa.gte(-0.005).and(tasa.lt(0.005)), 0)
                                .where(tasa.gte(0.005).and(tasa.lt(0.01)), 0.005)
                                .where(tasa.gte(0.01).and(tasa.lt(0.015)), 0.01)
                                .where(tasa.gte(0.015).and(tasa.lt(0.02)), 0.015)
                                .where(tasa.gte(0.02).and(tasa.lte(0.025)), 0.02)
                                .where(tasa.gte(0.025).and(tasa.lte(0.03)), 0.025)
                                .where(tasa.gt(0.03), 0.03);
            
                // parámetros de visualización
                var vis_ndvi = {min: 0, max: 1, palette: [
                'FFFFFF', 'CE7E45', 'FCD163', '66A000', '207401',
                '056201', '004C00', '023B01', '012E01', '011301'
                ]};
                var vis_tasa = {min: -0.03, max: 0.03, palette: ['b2182b','ef8a62','fddbc7','f7f7f7','d1e5f0',
                            '67a9cf','2166ac']};
                // genera thumbnail
                var thumb_path = tasa_res.visualize(vis_tasa).reproject({crs:'EPSG:3857',scale:10}).getThumbURL({dimensions:500,region:lote.toGeoJSON()})
                
                respuesta_obj.str_output_uri = thumb_path;
                respuesta_obj.str_gee_email = ee.data.getAuthClientId();
                
                let respuesta = JSON.stringify(respuesta_obj);
               // var queueService = azure.createQueueService(modulo_configuracion.configuracion.STORAGE_ACCOUNT_NAME, modulo_configuracion.configuracion.STORAGE_ACCOUNT_KEY);

                //enviamos la respuesta creada
                queueService.createMessage(modulo_configuracion.configuracion.buzon_escritura, respuesta, function(error) {

                console.log("Enviando mensaje al buzon");
                if (!error) {   
                    // Message escrito
                    console.log("mensaje " + respuesta+ " ha sido enviado");
                }else{
                    console.log("Se encontro un error al enviar el mensaje");
                }
                 });
            
             console.log(thumb_path);
                
                
        }
        else
        {
              respuesta_obj.str_output_uri = "Error no se ha encontrado imagen para la fecha enviada";
              respuesta_obj.str_gee_email = ee.data.getAuthClientId();

              let respuesta = JSON.stringify(respuesta_obj);
              //var queueService = azure.createQueueService(modulo_configuracion.configuracion.STORAGE_ACCOUNT_NAME, modulo_configuracion.configuracion.STORAGE_ACCOUNT_KEY);
              queueService.createMessage(modulo_configuracion.configuracion.buzon_escritura, respuesta, function(error) {
              console.log("Enviando mensaje al buzon");
              if (!error) {   
                  // Message escrito
                  console.log("mensaje " + respuesta+ " ha sido enviado");
              }else{
                  console.log("Se encontro un error al enviar el mensaje");
              }
             });
             console.log("Error No hay imagen para la fecha analizada");

        }

    }catch(err)
    {

        respuesta_obj.str_output_uri = "Error, no se pudo ejecutar la funcion gee "+err;
        respuesta_obj.str_gee_email = ee.data.getAuthClientId();

        let respuesta = JSON.stringify(respuesta_obj);
       // var queueService = azure.createQueueService(modulo_configuracion.configuracion.STORAGE_ACCOUNT_NAME, modulo_configuracion.configuracion.STORAGE_ACCOUNT_KEY);

        //enviamos la respuesta creada
        queueService.createMessage(modulo_configuracion.configuracion.buzon_escritura, respuesta, function(error) {

        console.log("Enviando mensaje al buzon");
        if (!error) {   
            // Message escrito
            console.log("mensaje " + respuesta+ " ha sido enviado");
        }else{
            console.log("Se encontro un error al enviar el mensaje");
        }
         });
    
        console.log("error");

    }
   

    });
});
}		

exports.obtener_tiles = obtener_tiles
exports.tasa_cambio = tasa_cambio
exports.azureInterface_obj = azureInterface_obj
