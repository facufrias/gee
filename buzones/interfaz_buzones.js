//var modulo_configuracion = require('./configuracion.js');  // este modulo contiene toda la informacion de config

var modulo_amazon = require ('./modulo_amazon/buzones.js');
var aws      = require('aws-sdk');
var modulo_azure = require ('./modulo_azure/buzones.js');
aws.config.loadFromPath(__dirname + '/config.json');
// Instantiate SQS.
var sqs = new aws.SQS();

function setMenssage(parametro='aws', mensaje,queue='https://sqs.us-west-2.amazonaws.com/942573785109/geotiler'){
    return (parametro === 'aws' ? modulo_amazon.send_queue(sqs,queue,mensaje) : modulo_azure.send_queue(mensaje,queueName));
}

function readMenssage(queueID,parametro='aws'){
    return (parametro === 'aws' ? modulo_amazon.read_queue(sqs,queueID) : modulo_azure.read_queue(queueID));
}

var mensaje = '{"str_userId":"ab2844fc-6ec7-4590-882b-1748090897a7","long_SvcProcessId":"98","str_start_date":"2018-03-11","str_end_date":"2018-03-21","str_bucket":"360_raster_pyramids_qa","str_python_function":"obtener_tiles","str_purpose":"service","str_blobContainerName":"svc-qa98","str_fileName":"lotes_2.geojson","str_sender":"bi","str_geojson_region":{ "type": "MultiPolygon", "coordinates": [ [ [ [ -64.376157, -27.133844 ], [ -64.368381, -27.135211 ], [ -64.36953, -27.139864 ], [ -64.377343, -27.138378 ], [ -64.376678, -27.135838 ], [ -64.376157, -27.133844 ] ] ] ] }}'

setMenssage("aws",mensaje)

readMenssage("https://sqs.us-west-2.amazonaws.com/942573785109/geotiler");

console.log(mensaje);
