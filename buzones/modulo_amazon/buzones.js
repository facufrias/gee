//var modulo_configuracion = require('./configuracion.js');  // este modulo contiene toda la informacion de config

var modulo_configuracion = require('../configuracion.js');
const modulo_servicios = require("../../servicios");  // este modulo contiene toda la informacion de config

//dado un mensaje queueUrl y un elemento de conexion sqs  envia un mensaje a la cola.



let send_queue = function send_queue_data(sqs,queueUrl,message)
{
  return new Promise( async function(resolve, reject) { 

      var params = {
        MessageBody: message,
        QueueUrl: queueUrl,
        DelaySeconds: 0
    };


    sqs.sendMessage(params, function(err, data) {
        if(err) {
            console.log(err)
            reject(err)
        } 
        else {
            resolve(data);
        } 
    });



  });
}

//dado un queueUrl y un elemento de conexion sqs, devuelve un mensaje y lo elimina de la cola.
var read_queue = function (sqs,queueUrl) {

    return new Promise( async function(resolve, reject) { 


    var params = {
        QueueUrl: queueUrl,
        VisibilityTimeout: 600 // 10 min wait time for anyone else to process.
    };


    sqs.receiveMessage(params, function(err, data) {
        if(err) {
           console.log(err);
           reject(err); // error al leer
        } 
        else if (data.Messages) {   // si existen los mensajes
            // creo los parametros para eliminar
            let message = data.Messages[0]
            var params ={
                QueueUrl: queueUrl,
                ReceiptHandle : data.Messages[0].ReceiptHandle               
            }
            sqs.deleteMessage(params, function(err, data){

                if(err){

                    reject(err);   // error de eliminacion
                }else{
                    console.log("Mensaje: " + message.Body); // salida anterior: console.log(data);

                }
            });
            resolve(message);
        }
    });



});
}
 



//exports.configuracion = configuracion



exports.send_queue = send_queue
exports.read_queue = read_queue


//exports.sendMessage = sendMessage

//exports.receiveMessage = receiveMessage

